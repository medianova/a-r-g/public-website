import Vue from 'vue';
import VueRouter from 'vue-router';

import HomePage from '../pages/HomePage.vue';
import EquipePage from '../pages/EquipePage.vue';
import LoginPage from '../pages/LoginPage.vue';
import BlueprintPage from '../pages/logs/BlueprintPage.vue';
import AudioPage from '../pages/logs/AudioPage.vue';
import GroupPicturesPage from '../pages/logs/GroupPicturesPage.vue';
import LogsPage from '../pages/logs/LogsPage.vue';
import PrivateFolderPage from '../pages/logs/PrivateFolderPage.vue';
import FinalPage from '../pages/FinalPage.vue';
import FinalGoodEndPage from '../pages/FinalGoodEndPage.vue';
import FinalBadEndPage from '../pages/FinalBadEndPage.vue';
import Error404Page from '../pages/Error404Page.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    alias: ['/index.html'],
    name: 'Home',
    component: HomePage,
  },
  {
    path: '/equipe',
    alias: ['/equipe.html'],
    name: 'Equipe',
    component: EquipePage,
  },
  {
    path: '/journal-de-bord',
    alias: ['/journal-de-bord.html'],
    name: 'Login',
    component: LoginPage,
  },
  {
    path: '/journal-de-bord/plan',
    alias: ['/journal-de-bord/plan.html'],
    name: 'Blueprints',
    component: BlueprintPage,
  },
  {
    path: '/journal-de-bord/enregistrements',
    alias: ['/journal-de-bord/enregistrements.html'],
    name: 'Audio',
    component: AudioPage,
  },
  {
    path: '/journal-de-bord/photos-groupe',
    alias: ['/journal-de-bord/photos-groupe.html'],
    name: 'GroupPictures',
    component: GroupPicturesPage,
  },
  {
    path: '/journal-de-bord/logs',
    alias: ['/journal-de-bord/logs.html'],
    name: 'Logs',
    component: LogsPage,
  },
  {
    path: '/journal-de-bord/dossier-prive',
    alias: ['/journal-de-bord/dossier-prive.html'],
    name: 'PrivateFolder',
    component: PrivateFolderPage,
  },
  {
    path: '/toutreposesurtoi',
    alias: ['/toutreposesurtoi.html'],
    name: 'Final',
    component: FinalPage,
  },
  {
    path: '/envoi-donnees',
    alias: ['/envoi-donnees.html'],
    name: 'FinalBadEnd',
    component: FinalBadEndPage,
  },
  {
    path: '/suppression-donnees',
    alias: ['/suppression-donnees.html'],
    name: 'FinalGoodEnd',
    component: FinalGoodEndPage,
  },
  {
    path: "*",
    name: '404', 
    component: Error404Page
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;